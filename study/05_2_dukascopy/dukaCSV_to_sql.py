import pandas as pd
import os
import sqlalchemy as sqla
import traceback


tablename = ""
# --- Config ----------------------------------------------------------------------------------------------------
pathToCSVFolder= "data/forex" #"data/forex"
sqlUrl         = 'postgresql://bachelor:ba2019SJ@bachelor.erzinger.net:5432/bachelor2'
# ---------------------------------------------------------------------------------------------------------------

# ----Functions--------------------------------------------------------------------------------------------------
#read the file, create dataframe
def readFile(filename):
    print('Read File: '+filename)
    print('Table: '+tablename+'. Reading....')
    df = pd.read_csv(pathToCSVFolder+'/'+filename, parse_dates=True, index_col='time', names=['time', 'ask', 'bid', 'ask_volume', 'bid_volume'], header=None)
    print('...reading success.')
    return df

#write to database
def writeToDb(datafr):
    try: 
        print('Connect Database...')
        engine = sqla.create_engine(sqlUrl)
        connection = engine.connect()
        print('...connected')
        print('Start writing to Tabel '+tablename)
        #print ('slicing data')
        #n = 1000000  #chunk row size
        #list_df = [datafr[i:i+n] for i in range(0,datafr.shape[0],n)]
        #for dfid in range(len(list_df)-1):
        #print('write chunk (' + str(dfid) + "/" + str(len(list_df)-1) + ")")
        datafr.to_sql(
            tablename, 
            connection, 
            if_exists='append', # {‘fail’, ‘replace’, ‘append’}, default ‘fail’
            index=True,
            chunksize=10000,
            dtype={ 'time': sqla.DateTime(timezone=False), 
                    'ask':  sqla.types.Float(precision=24),
                    'bid': sqla.types.Float(precision=24),
                    'ask_volume': sqla.types.INTEGER(),
                    'bid_volume': sqla.types.INTEGER()}
        )        
        print('Write SQL Success! Table: '+tablename)
    except:
        print('unknown error' + "\r\n\r\n" + str(traceback.format_exc()))
    finally:  
        try:
            connection.close()
        except:
            print()

# ---------------------------------------------------------------------------------------------------------------


#STARTER: for each file in folder
for root, dirs, files in os.walk(pathToCSVFolder):  
    for filename in files:
        tablename = filename[0:6]
        data = readFile(filename)
        writeToDb(data)