# time, ask, bid, ask_volume, bid_volume


#get with header
duka EURCHF --header

#1 year
duka EURCHF -s 2018-01-01 -e 2019-01-01 -t 1

#1 month
duka EURCHF -s 2018-01-01 -e 2018-02-01 -t 1 -f FOLDER

#1 month/ custom store folder
duka EURCHF -s 2018-01-01 -e 2018-02-01 -t 1 -f data/forex/
duka EURCHF EURUSD -s 2018-01-01 -e 2018-02-01 -t 1 -f data/forex/ #Auf Server getestet läuft einwandfrei in ca 30 sekunden durch!

#1MIN Candles
duka EURCHF EURUSD -s 2018-01-01 -e 2018-02-01 -t 1 -f data/forex/ -c 1M

'''
positional arguments:
  SYMBOLS               symbol list using format EURUSD EURGBP

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -d DAY, --day DAY     specific day format YYYY-MM-DD (default today)
  -s STARTDATE, --startdate STARTDATE
                        start date format YYYY-MM-DD (default today)
  -e ENDDATE, --enddate ENDDATE
                        end date format YYYY-MM-DD (default today)
  -t THREAD, --thread THREAD
                        number of threads (default 20)
  -f FOLDER, --folder FOLDER
                        destination folder (default .)
  -c CANDLE, --candle CANDLE
                        use candles instead of ticks. Accepted values 1M 5M
                        10M 15M 30M 1H 4H
  --header              include CSV header (default false)

'''