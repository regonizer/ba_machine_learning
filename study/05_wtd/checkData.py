import pandas as pd
import os
import sqlalchemy as sqla
import traceback

# --- Config ----------------------------------------------------------------------------------------------------
sqlUrl         = 'postgresql://bachelor:ba2019SJ@bachelor.erzinger.net:5432/bachelor2'
'''
select "ask","time" from "EURCHF" ORDER BY "time" DESC LIMIT 10000
'''
# ---------------------------------------------------------------------------------------------------------------

engine = sqla.create_engine(sqlUrl)
connection = engine.connect()
df = pd.read_sql('''
select
    ask, time,
    count(*) as count,
    date_part('hour', time) as hr
from "EURCHF"
where hr >= TIMESTAMP 'yesterday' AND created_at < TIMESTAMP 'today'
group by ask, hr
''', connection, index_col="time")
df.head()