#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import os
import datetime
import time
import pandas as pd
import sqlalchemy as sqla
import smtplib
import sys
import traceback
import threading

# --- Config ----------------------------------------------------------------------------------------------------
class Config():
    loglevel       = logging.INFO
    logMail        = True
    sqlUrl         = 'postgresql://bachelor:ba2019SJ@bachelor.erzinger.net:5432/bachelor2'
    sqlInterval    = 1 # writing interval between one table (>600) in seconds
    sqlReset       = False
    wtdInterval    = 5 # interval between two requests from six in seconds
    wtdSixFile     = os.path.join('data', 'six', 'six_sym.json')
    wtdSixOpen     = datetime.time(7, 59, 0) # six open time (08:00)
    wtdSixClose    = datetime.time(18, 1, 0) # six close time (18:00)
    #wtdToken      = 'NOCj3No1I1qlGzHoVG7qLlFxOgWLi2jK1MNgbcZc7RBf8enQ1DbJ5rbLKIdT' # SJ
    #wtdToken      = 'npeK0CbwL7wj5hs9Z6RYxXkKX1T7VDlJHJDLwsk3O23btbFBKQz5v1AteGVI' # SJ
    #wtdToken      = 'pJvWczZNwt6mVZHOLDM8qrN9HH7jI5n0xJMX2SQO3mc04P0xXiOvbNDnaBZZ' # random
    #wtdToken      = 'QX5X7EfPTb3n2yxzuPneaslfHtKJTpPnOp7CB9kbFwIE8O3jhCU5vm59Znjg' # JE
    wtdToken       = 'qUjmLPwIAuS4pzrDZbPf26r6HwkFSQU2QbMxdBvVeYevNuvUZ2DFTtCuujVA' # ba.aktienkurse@gmail.com
    wtdUrl         = 'https://www.worldtradingdata.com/api/v1/stock'
    sleepInterval  = 5 # waiting interval for sleep mode if six is closed
# ---------------------------------------------------------------------------------------------------------------


# --- Helper Classes --------------------------------------------------------------------------------------------
#
# Six informations
class Six():
    def open():
        return Six.timeInRange(Config.wtdSixOpen, Config.wtdSixClose, datetime.datetime.now().time())
    def timeInRange(start, end, x):
        """Return true if x is in the range [start, end]"""
        if start <= end:
            return start <= x <= end
        else:
            return start <= x or x <= end
#
# Delayer 
class Timer():
    def __init__(self):
        self.start()
    def start(self):
        self.starttime = datetime.datetime.now()
    def getTime(self):
        return (datetime.datetime.now() - self.starttime).total_seconds()
    def sleepTo(self, endelay):
        sleeptime = endelay - (datetime.datetime.now() - self.starttime).total_seconds()
        logging.getLogger().debug('wait ' + str(sleeptime) + 's')
        if (sleeptime > 0): 
            time.sleep(sleeptime)
        else: 
            logging.getLogger().warn(
                "overtime alert in thread " + 
                str(threading.current_thread().name) + " with %0.02fs", abs(sleeptime))
#
# Logger 
class CustomLogger():
    def __init__(self, name, level=logging.NOTSET): 
        self._lastmessage = datetime.datetime.now()
        self.format = "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s"
        self.errors = collections.deque(maxlen=6)
        self.threads = [[]]
    def addThread(self, thread):
        self.threads.append([thread, "starting"])
    def error(self, msg, *args, **kwargs):
        thead = threading.current_thread()
        self.__mail(msg)
        self.__print(_msg)
    def warn(self, msg, *args, **kwargs):

    def info(self, msg, *args, **kwargs):

    def __print(self):
        threads = [t for t in threads if threads.is_alive()]

    def __mail(self, message):
        if not Config.logMail: return
        time.sleep(10) # error message delay
        if ((datetime.datetime.now() - self._lastmessage).total_seconds() < 3600): return 
        self._lastmessage = datetime.datetime.now()
        logging.getLogger().info('sending notification mail')    
        sender = 'bot@erzinger.net'
        receivers = ['joel@erzinger.net', 'silvio.jaeger@ntb.ch']
        header  = 'From: %s\r\n' % sender
        header += 'To: %s\r\n' % ','.join(receivers)
        header += 'Cc:\r\n'
        header += 'Subject: Error by collecting data from wtd\r\n'
        message = header + message
        try:
            server = smtplib.SMTP('mail.erzinger.net', 587)
            server.starttls()
            server.login("bot@erzinger.net", "ba2019")
            server.sendmail(sender, receivers, message)
            server.quit()  
            logging.getLogger().info("successfully sent email")
        except:
            print("Error: unable to send email\r\n" + str(traceback.format_exc()))
# ---------------------------------------------------------------------------------------------------------------


# --- Collector -------------------------------------------------------------------------------------------------
# collects data from wtd in a given interval -> see config
class Collector:
    def __init__(self, logger):
        self.logger = logger
        self.logger.info('startup wtd listening')
        self.dataframeStore = {}
        self.timer = Timer()
        self.thread = threading.Thread(target=self.__run, name="collector")
    def start(self):
        self.running = True
        self.thread.start()
    def stop(self):
        self.running = False
        self.thread.join()
    def __run(self):
        self.__getStocks() # read the stock symbols 
        while self.running:
            try:
                self.timer.start()
                if not Six.open(): # sleep if six is closed
                    self.__cleanup()
                    self.timer.sleepTo(Config.sleepInterval)
                else: # request six data if six is open
                    for symbolsblock in self.symbols: 
                        requestTimestamp = pd.Timestamp.now()
                        response = self.__reqestLive(symbolsblock)
                        self.__writeToStore(symbolsblock, response, requestTimestamp)
                    self.logger.info("requests finished for %d symbols in %0.2fs", len(self.dataframeStore), self.timer.getTime())
                    self.timer.sleepTo(Config.wtdInterval)
            except:
                self.logger.error('unknown error' + "\r\n\r\n" + str(traceback.format_exc()))
    def __cleanup(self):
        for symbol, dataframe in self.dataframeStore.items():
            if not self.running: return
            if dataframe.size > 0: 
                self.logger.info("cleanup garbage for %s (SIX closed)", symbol)
                dataframe.drop(dataframe.index, inplace=True)
    def __getStocks(self):
        self.logger.info('loading ' + Config.wtdSixFile)
        with open(Config.wtdSixFile, 'r') as symfile: 
            symbolsJson = json.load(symfile)
            self.symbols = symbolsJson['symbols']
            for symbol in self.symbols:
                self.dataframeStore[symbol] = pd.DataFrame()
            self.symbols = [self.symbols[i:i+199] for i in range(0, len(self.symbols), 199)]
            self.logger.info('sliced into ' + str(len(self.symbols)) + ' blocks')
    def __reqestLive(self, symbols):
        symbols = ','.join(symbols)
        params = dict(
            symbol = symbols,
            sort_by = 'symbol',
            api_token = Config.wtdToken
        )
        resp = requests.get(url=Config.wtdUrl, params=params)
        json = resp.json()
        return json
    def __writeToStore(self, symbols, json, requestTimestamp):
        try:
            for symbol in json['data']:
                if not self.running: return
                try:
                    for attribute, value in symbol.items():
                        if value == "N/A": symbol[attribute] = "nan"
                    self.dataframeStore[symbol['symbol']] = self.dataframeStore[symbol['symbol']].append(pd.DataFrame(
                        index=[requestTimestamp],
                        data={ 
                            'price' : float(symbol['price']),
                            'volume' : float(symbol['volume']),
                            'shares' : float(symbol['shares']),
                            'market_cap' : float(symbol['market_cap']),
                            'last_trade_time' : pd.Timestamp(symbol['last_trade_time']),
                            'new' : True # for database write process, HAVE TO BE LAST ROW!
                        }
                    ))
                except:
                    self.logger.warn('unable to get/write ' + str(symbol['symbol']))
        except:
            self.logger.error('unable to get data' + "\r\n\r\n" + str(traceback.format_exc()))
#
# writes a pandas dataframe to the databse -> see config
class DatabaseHandler:
    def __init__(self, dataframs, logger):
        self.logger = logging.getLogger()
        self.timer = Timer()
        self.thread = threading.Thread(target=self.__run, name="dbhandler") 
    def start(self):
        self.running = True
        time.sleep(Config.wtdInterval)
        self.thread.start()
    def stop(self):
        self.running = False
        self.thread.join()
    def __run(self):
        self.logger.info('connecting to erzinger.net')
        self.engine = sqla.create_engine(Config.sqlUrl)
        while self.running: 
            self.timer.start()
            if Six.open(): 
                self.__writeToDatabase()
                self.timer.sleepTo(Config.sqlInterval)
            else:
                self.timer.sleepTo(Config.sleepInterval)
    def __writeToDatabase(self):
        if_exists = 'append'
        if Config.sqlReset:
            self.logger.info("replace existing tables")
            if_exists = 'replace'
            Config.sqlReset = False
        for symbol, dataframe in self.collector.dataframeStore.items():
            if not self.running: return
            self.timer.start()
            try:
                connection = self.engine.connect()
                if if_exists == 'replace': 
                    self.logger.info("whipe table (%d/%d) %s", 
                    list(self.collector.dataframeStore.keys()).index(symbol),
                    len(self.collector.dataframeStore.items()),
                    symbol)
                dataframe[dataframe['new']][dataframe.columns[:-1]].to_sql(
                    symbol, 
                    connection, 
                    if_exists=if_exists # {‘fail’, ‘replace’, ‘append’}, default ‘fail’
                ) 
                self.collector.dataframeStore[symbol] = dataframe.iloc[0:0]
                connection.close()
            except:
                self.logger.error('database writing error' + "\r\n\r\n" + str(traceback.format_exc()))
            self.logger.info("database write for %s finished in %0.2fs", symbol, self.timer.getTime())
            self.timer.sleepTo(Config.sqlInterval)
# ---------------------------------------------------------------------------------------------------------------


# --- Start -----------------------------------------------------------------------------------------------------
def main():
    try:
        print('press enter to quit ...')
        logging.setLoggerClass(CustomLogger)
        logging.getLogger().setLevel(level=Config.loglevel)
        logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        logging.getLogger().addHandler(consoleHandler)
        collector = Collector()
        collector.start()
        dbhandler = DatabaseHandler(collector)
        dbhandler.start()
        sys.stdin.readline()
    except:
        logging.getLogger().error('unknown error' + "\r\n\r\n" + str(traceback.format_exc()))
    finally:
        print('Stopping tasks ...')
        try: dbhandler.stop()
        except: print()
        try: collector.stop() 
        except: print()
#
if __name__ == "__main__": main()
# ---------------------------------------------------------------------------------------------------------------