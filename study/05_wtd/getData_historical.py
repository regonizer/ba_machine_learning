#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import os
import datetime
import time
import logging
import pandas as pd
import sqlalchemy as sqla
import smtplib
import sys
import traceback
import threading

# --- Config ----------------------------------------------------------------------------------------------------
class Config():
    loglevel       = logging.INFO
    logMail        = True
    wtdSixFile     = os.path.join('data', 'six', 'six_sym.json')
    sqlUrl         = 'postgresql://bachelor:ba2019SJ@bachelor.erzinger.net:5432/bachelor2'
    sqlReset       = True
    #wtdToken      = 'NOCj3No1I1qlGzHoVG7qLlFxOgWLi2jK1MNgbcZc7RBf8enQ1DbJ5rbLKIdT' # SJ
    #wtdToken      = 'npeK0CbwL7wj5hs9Z6RYxXkKX1T7VDlJHJDLwsk3O23btbFBKQz5v1AteGVI' # SJ
    #wtdToken      = 'pJvWczZNwt6mVZHOLDM8qrN9HH7jI5n0xJMX2SQO3mc04P0xXiOvbNDnaBZZ' # random
    #wtdToken      = 'QX5X7EfPTb3n2yxzuPneaslfHtKJTpPnOp7CB9kbFwIE8O3jhCU5vm59Znjg' # JE
    wtdToken       = 'qUjmLPwIAuS4pzrDZbPf26r6HwkFSQU2QbMxdBvVeYevNuvUZ2DFTtCuujVA' # ba.aktienkurse@gmail.com
    wtdUrl         = 'https://www.worldtradingdata.com/api/v1/history'
# ---------------------------------------------------------------------------------------------------------------


# --- Helper Classes --------------------------------------------------------------------------------------------
#
# Logger 
class CustomLogger(logging.Logger):
    def __init__(self, name, level=logging.NOTSET): 
        customLogger = super(CustomLogger, self).__init__(name, level)  
        self._lastmessage = time.time()
        self._lastmessageLock = threading.Lock() 
    def error(self, msg, *args, **kwargs):
        self.__mail(msg)
        return super(CustomLogger, self).error(msg, *args, **kwargs)
    def __mail(self, message):
        if not Config.logMail: return
        time.sleep(10) # error message delay
        if ((time.time() - self._lastmessage).total_seconds() < 3600): return 
        self._lastmessage = time.time()
        logging.getLogger().info('sending notification mail')    
        sender = 'bot@erzinger.net'
        receivers = ['joel@erzinger.net', 'silvio.jaeger@ntb.ch']
        header  = 'From: %s\r\n' % sender
        header += 'To: %s\r\n' % ','.join(receivers)
        header += 'Cc:\r\n'
        header += 'Subject: Error by collecting data from wtd\r\n'
        message = header + message
        try:
            server = smtplib.SMTP('mail.erzinger.net', 587)
            server.starttls()
            server.login("bot@erzinger.net", "ba2019")
            server.sendmail(sender, receivers, message)
            server.quit()  
            logging.getLogger().info("successfully sent email")
        except:
            print("Error: unable to send email\r\n" + str(traceback.format_exc()))
#
# ---------------------------------------------------------------------------------------------------------------


# --- Collector -------------------------------------------------------------------------------------------------
# grabber historical data
class Grabber:
    def __init__(self):
        self.logger = logging.getLogger()
        self.logger.info('starting grabber')
        self.dataframeStore = {}
        self.thread = threading.Thread(target=self.__run, name="grabber")
    def start(self):
        self.running = True
        self.thread.start()
    def stop(self):
        self.running = False
        self.thread.join()
    def __run(self):
        try:
            self.__getStocks() # read the stock symbols 
            self.logger.info('connecting to erzinger.net')
            engine = sqla.create_engine(Config.sqlUrl)
            self.connection = engine.connect()
            self.if_exists = 'append'
            if Config.sqlReset:
                self.logger.info("replace existing tables in all database operations")
                self.if_exists = 'replace'
                Config.sqlReset = False
            for symbol in self.symbols: 
                if not self.running: return
                self.logger.info("request symbol %s", symbol)
                response = self.__request(symbol)
                self.__writeToDatabse(symbol, response)
        except:
            self.logger.error('unknown error' + "\r\n\r\n" + str(traceback.format_exc()))
        finally:
            try: self.connection.close() 
            except: print()
    def __getStocks(self):
        self.logger.info('loading ' + Config.wtdSixFile)
        with open(Config.wtdSixFile, 'r') as symfile: 
            symbolsJson = json.load(symfile)
            self.symbols = symbolsJson['symbols']
            for symbol in self.symbols:
                self.dataframeStore[symbol] = pd.DataFrame()
            self.logger.info('' + str(len(self.symbols)) + ' symbols loaded')
    def __request(self, symbol):
        params = dict(
            symbol = symbol,
            sort = 'newest',
            #output = 'csv',
            api_token = Config.wtdToken
        )
        resp = requests.get(url=Config.wtdUrl, params=params)
        json = resp.json()
        return json
    def __writeToDatabse(self, symbol, json):
        try:
            dataframe = pd.DataFrame(
                index=[pd.Timestamp(date) for date in json['history']],
                data={ 
                    'open' : [float(json['history'][date]['open']) for date in json['history']],
                    'close' : [float(json['history'][date]['close']) for date in json['history']],
                    'high' : [float(json['history'][date]['high']) for date in json['history']],
                    'low' : [float(json['history'][date]['low']) for date in json['history']],
                    'volume' : [float(json['history'][date]['volume']) for date in json['history']],
                }
            )
            # write to database
            if self.if_exists == 'replace': 
                self.logger.info("whipe and rewrite table (%d/%d) %s", 
                    self.symbols.index(symbol),
                    len(self.symbols),
                    symbol)
            else:
                self.logger.info("write table (%d/%d) %s", 
                    self.symbols.index(symbol),
                    len(self.symbols),
                    symbol) 
            dataframe.to_sql(
                symbol, 
                self.connection, 
                if_exists=self.if_exists # {‘fail’, ‘replace’, ‘append’}, default ‘fail’
            ) 
        except:
            self.logger.error('unable to write data from %s \r\n\r\n %s', symbol, str(traceback.format_exc()))
# ---------------------------------------------------------------------------------------------------------------


# --- Start -----------------------------------------------------------------------------------------------------
def main():
    try:
        print('press enter to quit ...')
        logging.setLoggerClass(CustomLogger)
        logging.getLogger().setLevel(level=Config.loglevel)
        logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        logging.getLogger().addHandler(consoleHandler)
        grabber = Grabber()
        grabber.start()
        sys.stdin.readline()
    except:
        logging.getLogger().error('unknown error' + "\r\n\r\n" + str(traceback.format_exc()))
    finally:
        print('Stopping tasks ...')
        try: grabber.stop()
        except: print()
#
if __name__ == "__main__": main()
# ---------------------------------------------------------------------------------------------------------------