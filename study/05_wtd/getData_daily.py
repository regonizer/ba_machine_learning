import requests
import json
import os

## Heruntegeladenen Daten noch benennen, bereits geladenen Daten skippen, abbruch bei mehr als 250 requests am tag

# The request function
def reqestHistory(sym):
    url = "https://www.worldtradingdata.com/api/v1/history"
    params = dict(
        symbol=sym,
        sort='newest',
        #api_token='NOCj3No1I1qlGzHoVG7qLlFxOgWLi2jK1MNgbcZc7RBf8enQ1DbJ5rbLKIdT'
        #api_token='npeK0CbwL7wj5hs9Z6RYxXkKX1T7VDlJHJDLwsk3O23btbFBKQz5v1AteGVI'
        api_token='pJvWczZNwt6mVZHOLDM8qrN9HH7jI5n0xJMX2SQO3mc04P0xXiOvbNDnaBZZ'
    )
    resp = requests.get(url=url, params=params)
    return resp.json()

#read the stock symbols
print(os.getcwd())
with open('data/six/six_sym.json', 'r') as symfile: 
    syms = json.load(symfile)
    for symbol in syms['symbols']:
        path = os.path.join('data', 'wtd', 'daily', symbol+'.json')
        if os.path.isfile(path): continue
        print('download '+ symbol + ' as '+path)
        data = reqestHistory(symbol)
        with open(path, 'w') as outfile:
            json.dump(data, outfile)

#for symb in symList:
#    print('Symbol: '+symb)
#    actulJson = reqestHistory(symb)
#    
#    #write json to Filesystem
#    with open(symb+'.json', 'w') as outfile:
#        json.dump(actulJson, outfile)
    
#f.close()