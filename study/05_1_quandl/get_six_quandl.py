import quandl
import pandas
import csv
import sys

quandl.ApiConfig.api_key = "EQ-sbfwyC2u1TWWxdfuo"

# get six metadata
stocks = []
with open('data/six/SIX_metadata.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    stocks = [row for row in csv_reader]
    print(f'Column names are {", ".join(stocks[0])}')
    print(f'{stocks[1]}')

# get six stock data
startDate  = '2018-1-1'
endDate    = '2018-6-1'
data       = pandas.DataFrame(index=pandas.date_range(start=startDate, end=endDate))

for stock in stocks[1:3]:
    sys.stdout.write(f'get stocks for {stock[1]} (SIX/{stock[0]}) from {startDate} to {endDate} ... ')
    quandldata = quandl.get('SIX/'+stock[0], start_date=startDate, end_date=endDate)
    quandldata['link'] = f'SIX/{stock[0]}'
    quandldata['name'] = f'{stock[1]}'
    #data = pandas.concat([data, quandldata], axis=1, join_axes=[data.index])
    data = pandas.concat([data, quandldata], axis=0, sort=False)
    sys.stdout.write(f'done \r\n')

normalized = data.fillna(method='ffill').fillna(method='bfill').sort_index()
normalized.to_csv('data/six/SIX_data.csv')