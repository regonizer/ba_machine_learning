import csv

stocks = []
with open('data/SIX_metadata.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    stocks = [row for row in csv_reader]
    line = 0
    for row in stocks:
        if line == 0:
            print(f'Column names are {", ".join(row)}')
        if line == 1:
            print(f'eg. {row}')
        line += 1