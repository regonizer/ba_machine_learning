import quandl
import matplotlib.pyplot as plt
import numpy as np
import pandas
from sklearn import preprocessing

quandl.ApiConfig.api_key = "EQ-sbfwyC2u1TWWxdfuo"


#quandle codes for the stocks
stocks = [
    ["swissLifePr"  , "SIX/CH0014852781CHF.1"],
    ["nestlePr"     , "SIX/CH0038863350CHF.1"],
    ["swisscomPr"   , "SIX/CH0008742519CHF.1"],
    ["novartisPr"   , "SIX/CH0012005267CHF.1"]
]

#Get Only "Price" collum (Price is collum 1)
data = quandl.get([stock[1] for stock in stocks], start_date="2015-1-1", end_date="2018-6-1")

numpyarray = data.values
scaler = preprocessing.MinMaxScaler()
scaled = scaler.fit_transform(numpyarray)
datan = pandas.DataFrame(scaled)

# raw data
plt.plot(data)
plt.title("Stocks from SMI")
plt.legend([stock[0] for stock in stocks])

plt.matshow(datan.corr())
plt.title("Correlation")

# normalized data
plt.figure()
plt.plot(datan)
plt.title("Stocks normalized")
plt.legend([stock[0] for stock in stocks])
plt.show()


