import pandas as pd
import tensorflow
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import LSTM,Dense
from keras.callbacks import TensorBoard
import numpy as np

#Load data
symbols = ['SCMN.SW', 'NOVN.SW', 'UBSG.SW']
df = pd.read_csv('data/wtd/history/'+symbols[1]+'.csv').set_index('Date')
df['Mid'] = (df['Low']+df['High'])/2

#visualize data
def plot(df):
    plt.figure(figsize = (18,9))
    plt.plot(range(df.shape[0]), (df['Low']+df['High'])/2)
    plt.xticks(range(0,df.shape[0],20), df.index[::20], rotation=90)
    plt.grid(True)
    plt.xlabel('Date', fontsize=18)
    plt.ylabel('Mid Price', fontsize=18)
    plt.show()

def plotKeras(prediction, truth):
    plt.figure(figsize = (18,9))
    plt.plot(range(len(prediction)), prediction, label='prediction')
    plt.plot(range(len(truth)), truth, label='truth')
    plt.grid(True)
    plt.xlabel('Data', fontsize=18)
    plt.ylabel('Mid Price', fontsize=18)
    plt.legend(['prediction', 'truth'])
    plt.show()

#scale the data between 0 and 1 !!NOT PERMITTED!! We also scale the test data here!
scaler = MinMaxScaler()
values = df['Mid'].values
shaped = values.reshape(values.shape[0], 1)
scaled_data = scaler.fit_transform(shaped)
print('scaled data:')
print(scaled_data)

# split train and testdata (80 : 20)
count_row = scaled_data.shape[0]  # gives number of row count
train_rows = int(count_row*0.8)

train_data = scaled_data[:train_rows]
test_data = scaled_data[train_rows:]
print("TrainData: \r\n%s\r\n" % train_data[:4])
print("TestData: \r\n%s\r\n" %  test_data[:4])

def sliceData(data,blocksize):    #slices data into every day view
    X,Y = [],[]
    for i in range(len(data)-blocksize-1):
        X.append(data[i:(i+blocksize),0]) #actual block eg last week
        Y.append(data[(i+blocksize),0])  #the next day
    X = np.array(X)
    X = X.reshape((X.shape[0], X.shape[1], 1))
    Y = np.array(Y)
    return X, Y

train_x, train_y = sliceData(train_data, 7)
test_x, test_y = sliceData(test_data, 7)


def demoOfSlicedata():
    a= [[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]]
    mean_data = np.array(a)
    print('Sliced Data:')
    print(sliceData(mean_data, 7))

demoOfSlicedata()

# creat the model------------------------------------------------------------------------------------
d = 1 # Dimensionality of the data. Since your data is 1-D this would be 1
num_unrollings = 50 # Number of time steps you look into the future.
batch_size = 500 # Number of samples in a batch
num_nodes = [128,128,128] # Number of hidden nodes in each layer of the deep LSTM stack we're using
n_layers = len(num_nodes) # number of layers
dropout = 0.2 # dropout amount-----------------------------------------------------------------------

#Tensorboard
tensorboard = TensorBoard(log_dir='./logs', histogram_freq=0,
                          write_graph=True, write_images=False)

#create the model
model = Sequential()
model.add(LSTM(256,input_shape=(7,1)))
model.add(Dense(1))

#Compile Model
model.compile(optimizer='adam',loss='mse', metrics=['accuracy'])

#train the model
model.fit(train_x, train_y, epochs=200, shuffle=False, callbacks=[tensorboard])
model.save("models/model_keras_tf_lstm_daily.model", overwrite=True, include_optimizer=True)

#test the model with our testdata
pred = model.predict(test_x)
plotKeras(pred, test_y)

#Start Tenserboard via terminal with: "tensorboard --logdir ./logs"