import json
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def jsonToDataframe(path):
    with open(path) as file:
        json1 = json.load(file)
    data = [[]]
    for r in json1['history']:
        data += [ pd.Timestamp(r), json1['history'][r]['open'] ]
    return pd.DataFrame(
        index=[pd.Timestamp(date) for date in json1['history']],
        data={ 
            'open' : [float(json1['history'][date]['open']) for date in json1['history']],
            'close' : [float(json1['history'][date]['close']) for date in json1['history']],
            'high' : [float(json1['history'][date]['high']) for date in json1['history']],
            'low' : [float(json1['history'][date]['low']) for date in json1['history']],
            'volume' : [float(json1['history'][date]['volume']) for date in json1['history']],
         }
    )

print('working dir: '+os.getcwd())
with open('data/six_sym.json', 'r') as symfile: 
    syms = json.load(symfile)

df1 = jsonToDataframe(os.path.join('data', 'wtd', 'SCMN.SW.json'))
df2 = jsonToDataframe(os.path.join('data', 'wtd', 'SRCG.SW.json'))

idx = pd.date_range(df1.index.min(), df1.index.max(), freq='D')

df1_norm = df1.reindex(idx, fill_value=None).fillna(method='ffill').fillna(0)
df2_norm = df2.reindex(idx, fill_value=None).fillna(method='ffill').fillna(0)

x = np.diff(df1_norm['open'].values)[-1000:]
y = np.diff(df2_norm['open'].values)[-1000:]

corr = np.correlate(x, y, mode='same')
plt.plot(x)
plt.plot(y)
plt.show()
plt.plot(corr)
plt.show()