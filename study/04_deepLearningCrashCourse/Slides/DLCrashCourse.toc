\select@language {ngerman}
\beamer@sectionintoc {1}{Einf\"uhrung}{3}{0}{1}
\beamer@sectionintoc {2}{Supervised Learning}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{Datenmodellierung}{18}{0}{2}
\beamer@subsectionintoc {2}{2}{Algorithmische Modellierung}{25}{0}{2}
\beamer@sectionintoc {3}{Neuronale Netze}{43}{0}{3}
