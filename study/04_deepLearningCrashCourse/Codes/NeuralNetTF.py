# -*- coding: utf-8 -*-
"""
Neural Net with Tensor Flow

Created on Tue Dec  4 17:15:19 2018

@author: frik
"""


import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

rng = np.random

# Parameters
learning_rate = 0.0001
training_epochs = 100000
display_step = 50


#def neuron_layer(X, n_neurons, name, activation=None):
#    with tf.name_scope(name):
#        n_inputs = int(X.shape[1])
#        stddev = 2/np.sqrt(n_inputs)
#        #init the weights (magic!)
#        init = tf.truncated_normal((n_inputs, n_neurons), stddev=stddev)
#        W = tf.Variable(init, name="weights")
#        # bias neuron with a connection to all neurons
#        b = tf.Variable(tf.zeros([n_neurons]), name="bias")
#        # compute the superimposed signales
#        Z = tf.matmul(X, W) + b
#        # if there is an activation function, apply it!
#        if activation is not None:
#            return activation(Z)
#        else:
#            return Z
        
#def neural_net_model(X):
#    
#    n_hidden1 = 10
#    n_hidden2 = 10
#    
#    # Construct a (deep) neural net
#    with tf.name_scope("dnn"):
#        h1 = tf.layers.dense(X, n_hidden1, activation=tf.nn.relu)
#        h2 = tf.layers.dense(h1, n_hidden2, activation=tf.nn.relu)
#        return tf.layers.dense(h2, 1)
        
        

# load the data set
Prestige = pd.read_csv("Prestige.csv")

# drop some features
Prestige.drop(['Unnamed: 0', 'census', 'type'], inplace=True, axis=1)

'''
neural net model for income as a function of eduction, women and prestige.
Find Parameters by minimizing the MSE loss function
'''

# prepare the data
scaler = StandardScaler()
X_raw = scaler.fit_transform(Prestige.drop('income', axis=1).values.reshape(-1,3))
Y_raw = Prestige['income'].values.reshape(-1,1)

# create test and train set
train_X, test_X, train_Y, test_Y = train_test_split(X_raw, Y_raw, test_size=0.2)

# tf Graph Input
X = tf.placeholder("float32", shape=(None, 3))
Y = tf.placeholder("float32")


n_samples = train_X.shape[0]

    
n_hidden1 = 10
n_hidden2 = 10
    
# Construct a (deep) neural net
with tf.name_scope("dnn"):
    h1 = tf.layers.dense(X, n_hidden1, activation=tf.nn.relu)
   # h2 = tf.layers.dense(h1, n_hidden2, activation=tf.nn.relu)
    pred = tf.layers.dense(h1, 1)

#pred = neural_net_model(X)

with tf.name_scope("loss"):
    # Mean squared error
    #cost = tf.reduce_sum(tf.pow(pred-Y, 2))/(2*n_samples)
    cost = tf.losses.mean_squared_error(pred, Y)

# Gradient descent
#  Note, minimize() knows to modify W and b because Variable objects are trainable=True by default
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# Start training
with tf.Session() as sess:
    
    # Run the initializer
    sess.run(init)
    
    # Fit all training data
    for epoch in range(training_epochs):
        
        #for (x, y) in zip(train_X, train_Y):
        sess.run(optimizer, feed_dict={X: train_X, Y: train_Y})
       
        # Display logs per epoch step
        if (epoch+1) % display_step == 0:
            c = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(c))

        
    print("Optimization Finished!")
    training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
    train_Y_hat = sess.run(pred, feed_dict={X: train_X})
    print("Training cost=", training_cost, '\n')

    # Graphic display: measured vs. predicted
    plt.plot(train_Y, train_Y_hat, '.', label='train')
    plt.grid()
    plt.xlabel('measured')
    plt.ylabel('fitted')
    plt.show()

     # Testing example
    print("Testing... ")
    testing_cost = sess.run(cost, feed_dict={X: test_X, Y: test_Y})  # same function as cost above
    print("Testing cost (MSE)=", testing_cost)
    print("Root mean square loss :", np.sqrt(testing_cost), "$")
    test_Y_hat = sess.run(pred, feed_dict={X: test_X})
    plt.plot(test_Y, test_Y_hat, '.', label="test")
    plt.grid()
    plt.xlabel('measured')
    plt.ylabel('fitted')
    plt.legend()
    plt.show()
