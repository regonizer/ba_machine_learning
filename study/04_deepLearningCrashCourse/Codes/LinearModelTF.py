# -*- coding: utf-8 -*-
"""
Linear Model with Tensor Flow

Created on Tue Dec  4 17:15:19 2018

@author: frik
"""


import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

rng = np.random

# Parameters
learning_rate = 0.007
training_epochs = 10000
display_step = 50

# load the data set
Prestige = pd.read_csv("Prestige.csv")

# drop some features
Prestige.drop(['Unnamed: 0', 'census', 'type'], inplace=True, axis=1)

'''
linear model for income as a function of eduction:

    income = beta_0 + beta_1 * education + eps
    
Find beta_0 and beta_1 by minimizing the loss function
    
    MSE = sum(income_i - beta_0 - beta_1 * education_i)^2    
'''

# prepare the data
scaler = StandardScaler()
X = scaler.fit_transform(Prestige.drop('income', axis=1).values.reshape(-1,3))
Y = Prestige['income'].values.reshape(-1,1)

# create test and train set
train_X, test_X, train_Y, test_Y = train_test_split(X, Y, test_size=0.2)


n_samples = train_X.shape[0]

# tf Graph Input
X = tf.placeholder("float32", shape=(None, 3))
Y = tf.placeholder("float32")

# Set model weights
W = tf.Variable(tf.random_normal(shape=(3,1), dtype=tf.float32), name="weight")
b = tf.Variable(tf.random_normal(shape=(1,1), dtype=tf.float32), name="bias")

# Construct a linear model
pred = tf.add(tf.matmul(X, W), b)

# Mean squared error
#cost = tf.reduce_sum(tf.pow(pred-Y, 2))/(2*n_samples)
cost = tf.losses.mean_squared_error(pred, Y)

# Gradient descent
#  Note, minimize() knows to modify W and b because Variable objects are trainable=True by default
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# Start training
with tf.Session() as sess:
    
    # Run the initializer
    sess.run(init)
    
    # Fit all training data
    for epoch in range(training_epochs):
        
        #for (x, y) in zip(train_X, train_Y):
        sess.run(optimizer, feed_dict={X: train_X, Y: train_Y})
        c = sess.run(cost, feed_dict={X: train_X, Y:train_Y})
         
        # Display logs per epoch step
        if (epoch+1) % display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(c), \
                "W=", sess.run(W), "b=", sess.run(b))

        
    print("Optimization Finished!")
    training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
    beta = sess.run(W)
    beta_0 = sess.run(b)
    train_Y_hat = sess.run(pred, feed_dict={X: train_X, W: beta, b: beta_0})
    print("Training cost=", training_cost, "W=", sess.run(W), "b=", sess.run(b), '\n')

    # Graphic display: measured vs. predicted
    plt.plot(train_Y, train_Y_hat, '.', label='train')
    plt.grid()
    plt.xlabel('measured')
    plt.ylabel('fitted')
    plt.show()

     # Testing example
    print("Testing... ")
    testing_cost = sess.run(tf.reduce_sum(tf.pow(pred - Y, 2)) / (2 * test_X.shape[0]), feed_dict={X: test_X, Y: test_Y})  # same function as cost above
    print("Testing cost (MSE)=", testing_cost)
    print("Root mean square loss :", np.sqrt(testing_cost), "$")
    test_Y_hat = sess.run(pred, feed_dict={X: test_X, W: beta, b: beta_0})
    plt.plot(test_Y, test_Y_hat, '.', label="test")
    plt.grid()
    plt.xlabel('measured')
    plt.ylabel('fitted')
    plt.legend()
    plt.show()
