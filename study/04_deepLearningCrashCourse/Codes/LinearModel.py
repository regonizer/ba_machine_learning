# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 23:14:37 2018

@author: Klaus Frick
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def scatter_line(x,y,beta_0, beta_1,ax=None):
    
    if ax is None:
        _, ax = plt.subplots()
        
    ax.plot(x, y, '.')
    min_x = np.min(x) - x.std()*0.1
    max_x = np.max(x) + x.std()*0.1
    ax.plot([min_x, max_x], [min_x*beta_1 + beta_0, max_x*beta_1 + beta_0])
    
    return ax
    
# load the data set
Prestige = pd.read_csv("Prestige.csv")

# drop some features
Prestige.drop(['Unnamed: 0', 'census', 'type'], inplace=True, axis=1)

# scatterplot of education vs. income
Prestige.plot.scatter('education', 'income')

'''
linear model for income as a function of eduction:

    income = beta_0 + beta_1 * education + eps
    
Find beta_0 and beta_1 by minimizing the loss function
    
    MSE = sum(income_i - beta_0 - beta_1 * education_i)^2    
'''

# prepare the data
X = np.ones((Prestige.shape[0], 1))
X = np.concatenate((X, Prestige['education'].values.reshape(-1,1)), axis=1)
y = Prestige['income'].values.reshape(-1,1)

# method 0: eyeballing
beta_0 = np.linspace(-3800, -2200, 300)
beta_1 = np.linspace(850, 950, 300)
[B0, B1] = np.meshgrid(beta_0, beta_1)

# define the energy function
def MSE_loss(X, y, beta_0, beta_1):
    y_hat = beta_0 + beta_1*X[:,1]
    return ((y.reshape(-1,1) - y_hat.reshape(-1,1))**2).mean()
    
# vectorize the function
vMSE_loss = np.vectorize(MSE_loss, excluded=(0, 1))
val =  vMSE_loss(X, y, B0, B1)
plt.contour(beta_0, beta_1, val, 50)
plt.pcolor(beta_0, beta_1, val, alpha = 0.1)
plt.xlabel('beta_0')
plt.ylabel('beta_1')
plt.title('MSE Loss Function')
plt.colorbar()


# method 1: solve the normal equations
beta = np.dot(np.dot(np.linalg.inv(np.dot(X.T, X)), X.T), y)
print(beta)
# beta_0 = -2853.58$
# beta_1 = 898.81$ per year
# scatterplot of education vs. income
ax = scatter_line(X[:,1], y, beta[0], beta[1])
ax.set_xlabel('Education (years)')
ax.set_ylabel('Income ($)')



# method 2: batch gradient descent

# gradient function of the MSE
def grad_mse(beta, X, y):
    n = X.shape[0]
    beta = beta.reshape(-1,1)
    X = X.reshape(-1,2)
    y = y.reshape(-1,1)
    return(2/n*np.dot(X.T, np.dot(X,beta)-y))


# learning rate
eta = 0.005
n_iterations = 10000
beta = np.array([-3000, 850]).reshape(-1,1)

plt.contour(beta_0, beta_1, val, 50)
plt.pcolor(beta_0, beta_1, val, alpha = 0.1)
plt.xlabel('beta_0')
plt.ylabel('beta_1')
plt.title('MSE Loss Function')
plt.colorbar()

plt.plot([-2853.58, -2853.58], [850, 950], 'r')
plt.plot([-3800, -2200], [898.58, 898.58], 'r')


for iter in range(n_iterations):
    beta = beta - eta*grad_mse(beta, X, y)
    
    if iter%100==0:
        plt.plot(beta[0], beta[1], 'ow')
plt.show()

# method 3: stochastic gradient descent

n_epochs = 100
beta = np.array([-3000, 850]).reshape(-1,1)
m = X.shape[0]

for epoch in range(n_epochs):
    for i in range(m):
        idx = np.random.randint(m)
        x_ = X[idx,:]
        y_ = y[idx]
        grad = grad_mse(beta, x_, y_)
        
        beta = beta - eta*grad_mse(beta, X, y)
        
    plt.plot(beta[0], beta[1], 'or')
        
        
