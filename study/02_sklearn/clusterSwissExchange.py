import quandl
import matplotlib.pyplot as plt
import numpy as np
import pandas
from sklearn import preprocessing
import csv

quandl.ApiConfig.api_key = "EQ-sbfwyC2u1TWWxdfuo"

stocks = []
with open('data/SIX_metadata.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    stocks = [row for row in csv_reader]
    line = 0
    for row in stocks:
        if line == 0:
            print(f'Column names are {", ".join(row)}')
        if line == 1:
            print(f'{row}')
        line += 1


data = quandl.get(['SIX/' + stock[0] for stock in stocks], start_date="2015-1-1", end_date="2018-6-1")

scaler = preprocessing.MinMaxScaler()
scaled = scaler.fit_transform(data.values)

diff = np.diff(scaled, n=1, axis=0)

plt.plot(diff)
plt.title("Differential")
plt.legend([stock[0] for stock in stocks])

plt.matshow(pandas.DataFrame(diff).corr('kendall'))
plt.title("Money Maker")
plt.show()