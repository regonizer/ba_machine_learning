import quandl
import matplotlib.pyplot as plt
import numpy as np
import pandas
from sklearn import preprocessing

quandl.ApiConfig.api_key = "EQ-sbfwyC2u1TWWxdfuo"


#quandle codes for the stocks
stocks = [
    ["swissLifePr"  , "SIX/CH0014852781CHF.1"],
    ["nestlePr"     , "SIX/CH0038863350CHF.1"],
    ["swisscomPr"   , "SIX/CH0008742519CHF.1"],
    ["novartisPr"   , "SIX/CH0012005267CHF.1"]
]

#Get Only "Price" collum (Price is collum 1)
data = quandl.get([stock[1] for stock in stocks], start_date="2015-1-1", end_date="2018-6-1")

scaler = preprocessing.MinMaxScaler()
scaled = scaler.fit_transform(data.values)

diff = np.diff(scaled, n=1, axis=0)

plt.plot(diff)
plt.title("Differential")
plt.legend([stock[0] for stock in stocks])

plt.matshow(pandas.DataFrame(diff).corr('kendall'))
plt.title("Korrelation")
plt.show()